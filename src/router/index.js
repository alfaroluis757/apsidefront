import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../pages/index'
import NotFound from '../pages/NotFound'
import MainLayout from '../layouts/MainLayout'

Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    component: MainLayout,
    children:[
      { path: '', component: index, name: "Planetas" },
    ]
  },
  {
    path: '/*',
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes
})


export default router;
