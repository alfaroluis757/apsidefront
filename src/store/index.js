import Vue from 'vue'
import Vuex from 'vuex'


import Planets from './Planets'

Vue.use(Vuex)


export default function () {
    const Store = new Vuex.Store({
      modules: {
        Planets
      },
      strict: process.env.DEBUGGING
    })
  
    return Store
}
  