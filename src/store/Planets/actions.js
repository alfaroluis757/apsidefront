
import axios from 'axios';

export function getPlanets({ commit, state }, data) {
    return new Promise((resolve, reject) => {
        commit('setPlanets', []);
        // let url = 'https://swapi.dev/api/planets/';
        
        let url = state.items.find(element=>{
            return element.id == state.url
        }).url

        if(state.pageActive != 1){
            url = url + "?page=" + state.pageActive;
        }


            
        axios.get(url).then(response => {

            const { results, count, next, previous } = response.data;
            const planets = results.map(element => {
                return {
                    name: element.name,
                    diameter: element.diameter,
                    climate: element.climate,
                    terrain: element.terrain
                }
            });

            commit('setPlanets', planets);
            commit('setPlanetsAll', planets);
            commit('setNpages', count / results.length);
            commit('setPreviousNext', { next: next, previous: previous });
            resolve()
        }).catch(error => {
            console.log("HA OCURRIDO UN ERROR");
            reject()
        })

    })
}

