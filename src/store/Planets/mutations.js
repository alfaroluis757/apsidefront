export function setPlanets(state, data){
    state.planets = data;
}

export function setPlanetsAll(state, data){
    state.planetsAll = data;
}

export function setNpages(state, data){
    state.Npages = data;
}

export function setActive(state, data) { 
    state.pageActive = data
}

export function setUrl(state, data) { 
    state.url = data
}


export function setPreviousNext(state, data) { 
    state.previous = data.previous
    state.next = data.next
}