

export function getPlanets(state) { 
    return state.planets
}
export function getPlanetsAll(state) { 
    return state.planetsAll
}
export function getNpages(state) { 
    return state.Npages
}
export function getActive(state) { 
    return state.pageActive
}
export function getPrevious(state) { 
    return state.previous
}
export function getNext(state) { 
    return state.next
}
export function getUrl(state) { 
    return state.url
}
export function getUrls(state) { 
    return state.items
}