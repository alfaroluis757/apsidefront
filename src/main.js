import Vue from 'vue'
global.axios = require('axios');

import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import store from './store'

new Vue({
  el: '#app',
  store,
  router,
  vuetify,
  components: { App },
  template: '<App/>'
})
